let originalImg; // = getCanvas();
var isDrawing;
var ctx;

function resetEfects(){
	ctx.drawImage(image(originalImg), 0, 0);
}
function resetSliders() {
	document.getElementById("ctrl1").value = "100"; //saturation
	document.getElementById("ctrl2").value = "0"; //blur
	document.getElementById("ctrl3").value = "0"; //hue
	document.getElementById("ctrl4").value = "100"  //brightness

}

const initPage = function () {
	resetSliders();

	if (originalImg !== undefined) {
		load(originalImg);
		//for some reason originalImg always passes this check and instead of 
		//reversing the image back to the original
	}
}

function saveEfect() {
	var saturationAmount = document.getElementById("ctrl1").value; //saturation
	var blurAmount = document.getElementById("ctrl2").value; //blur
	var hueAmount = document.getElementById("ctrl3").value; //hue
	var brightnessAmount = document.getElementById("ctrl4").value; //brightness

	applyFilters(saturationAmount, blurAmount, hueAmount, brightnessAmount);
	resetSliders();
}

function applyFilters(saturation, blurAmount, hueAmount, brightnessAmount) {
	console.log(`Apply filters was called with the params: ${saturation},${blurAmount},${hueAmount},${brightnessAmount}`);
	const canvas = document.getElementById('image_canvas');
	const ctx = canvas.getContext('2d');
	const imageURL = canvas.toDataURL();
	let image = new Image();
	image.src = imageURL;

	// //ctx.filter = `blur(${blurAmount}) saturate(${saturation}%) hue-rotate(${hueAmount}deg) brightness(${brightnessAmount})`;
	// ctx.drawImage(image, 0, 0);
	ctx.filter = `hue-rotate(${hueAmount}deg)`;
	ctx.drawImage(image, 0, 0);
	// ctx.filter = `brightness(${brightnessAmount})`;
	// ctx.drawImage(image, 0, 0);
	applyBlur(blurAmount);
	//ctx.filter = `saturate(${saturation}%)`;
	applySaturation(saturation);
	// //??? applying just one filter works, applying multiple filters will make it worse 
	//why??
	//and why the effect will be show after clicking on apply twice?
	// applyBrightness(brightnessAmount);

}

function applySaturation(amount){
	const canvas= document.getElementById('image_canvas');
	const ctx= canvas.getContext('2d');

	ctx.filter=`saturate(${amount}%)`;
	ctx.drawImage(image(canvas),0,0);
}

function applyBlur(amount) {
	console.log(`Apply filters was called with the params:${amount}`);
	const canvas = document.getElementById('image_canvas');
	const ctx = canvas.getContext('2d');

	ctx.filter = `blur(${amount}px)`;
	ctx.drawImage(image(canvas), 0, 0);
}

function applyBrightness(amount) {
	console.log(`Apply filters was called with the params:${amount}`);
	const canvas = document.getElementById('image_canvas');
	const ctx = canvas.getContext('2d');

	ctx.filter = `brightness(${amount})`;
	ctx.drawImage(image(canvas), 0, 0);
}



function image(canvas) {
	const imageURL = canvas.toDataURL();
	let image = new Image();
	image.src = imageURL;
	return image;
}

function applyImgToCanvas(img) {
	var canvas = document.getElementById("image_canvas");
	var ctx = canvas.getContext("2d");
	// call ctx.drawImage when the image got loaded
	img.onload = function () {
		// ctx.drawImage(img, 0, 0);
		var canvas = document.getElementById("image_canvas");
		canvas.width = (img.width > 500) ? 500 : img.width;
		canvas.height = (img.height > 500) ? 500 : img.height;
		ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, canvas.width, canvas.height); // stretch img to canvas size

		console.info(`old img size is: ${img.width}x${img.height}`)
		console.info(`new img size is: ${canvas.width}x${canvas.height}`)

	}
}

function loadCanvasImgage(file) {

	var img = new Image();
	img.src = (window.webkitURL ? webkitURL : URL).createObjectURL(file);
	originalImg = new Image();
	originalImg.src = (window.webkitURL ? webkitURL : URL).createObjectURL(file);

	applyImgToCanvas(img);
}

download_img = function (el) {
	var imageurl = getCanvas().toDataURL("image/png");
	el.href = imageurl;
};



initPage();

function getCanvas() {
	return document.getElementById("image_canvas");
}

function Draw(x, y, isDown) {
	if (isDown) {

		ctx.beginPath();
		ctx.strokeStyle = $('#selColor').val();
		ctx.lineWidth = $('#selWidth').val();
		ctx.lineJoin = "round";
		ctx.moveTo(lastX, lastY);
		ctx.lineTo(x, y);
		ctx.closePath();
		ctx.stroke();
	}
	lastX = x; lastY = y;
}

function clearArea() {
	// Use the identity matrix while clearing the canvas
	ctx.setTransform(1, 0, 0, 1, 0, 0);
	ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
}


function drawFigure() {
	ctx = getCanvas().getContext('2d');
	ctx.beginPath();
	ctx.arc(100, 100, 50, 0.25 * Math.PI, 1.25 * Math.PI, false);
	ctx.fillStyle = "rgb(255, 255, 0)";
	ctx.fill();
	ctx.beginPath();
	ctx.arc(100, 100, 50, 0.75 * Math.PI, 1.75 * Math.PI, false);
	ctx.fill();
	ctx.beginPath();
	ctx.arc(100, 75, 10, 0, 2 * Math.PI, false);
	ctx.fillStyle = "rgb(0, 0, 0)";
	ctx.fill();
}
